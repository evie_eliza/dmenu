#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/time.h>

#include <stdio.h>

#include <X11/Xlib.h>

#include "chord.h"

void keylist_init(int size) {
    recent_keys.size = size;
    recent_keys._curr_size = 0;
    recent_keys._keys = (KeySym*) malloc(size * sizeof(KeySym));
    recent_keys._times = (time_t*) malloc(size * sizeof(time_t));
}

bool key_index(KeySym sym, int *index) {
    int i, probe;

    if (!recent_keys._curr_size)
        return false;

    for (i = 1; i <= recent_keys._curr_size; ++i) {
        probe = abs(recent_keys._next - i) % recent_keys.size;
        if (recent_keys._keys[probe] == sym) {
            *index = probe;
            return true;
        }
    }
    return false;
}

void add_recent_key(KeySym sym) {
    struct timeval time;

    recent_keys._keys[recent_keys._next] = sym;
    gettimeofday(&time, NULL);
    recent_keys._times[recent_keys._next] = time.tv_usec;
    recent_keys._next = (recent_keys._next + 1) % recent_keys.size;

    if (recent_keys._curr_size < recent_keys.size)
        ++recent_keys._curr_size;
}

/*
 * Returns whether key represented by `sym` has been pressed within the last `delay` seconds
 */
bool is_key_recent(KeySym sym, double delay) {
    int i;
    struct timeval now;

    if (!key_index(sym, &i))
        return false;

    gettimeofday(&now, NULL);
    return (now.tv_usec - recent_keys._times[i]) < delay * 1000000;
}
