#ifndef CHORD_H
#define CHORD_H

#include <stdbool.h>
#include <stdlib.h>
#include <sys/time.h>

#include <X11/keysym.h>

typedef struct {
    int size, _curr_size, _next;
    KeySym *_keys;
    suseconds_t *_times;
} keylist;

keylist recent_keys;

void keylist_init(int size);
void add_recent_key(KeySym sym);
bool is_key_recent(KeySym sym, double delay);

#endif
