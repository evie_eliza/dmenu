dmenu - dynamic menu (Evie's fork)
====================
dmenu is an efficient dynamic menu for X.

Changes I made
--------------

### Configuration

* Colorscheme (light and dark modes)
* Font
* Keybindings

### Other things
* Chords: type multiple keys at once to invoke some action! Modeled on [key-chord.el](https://emacswiki.org/emacs/key-chord.el)

Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dmenu is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dmenu
(if necessary as root):

    make clean install


Running dmenu
-------------
See the man page for details.
